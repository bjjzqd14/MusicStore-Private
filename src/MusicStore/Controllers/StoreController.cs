using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using MusicStore.Models;

namespace MusicStore.Controllers
{
    public class StoreController : Controller
    {
        private readonly AppSettings _appSettings;

        public StoreController(MusicStoreContext dbContext, IOptions<AppSettings> options)
        {
            DbContext = dbContext;
            _appSettings = options.Value;
        }

        public MusicStoreContext DbContext { get; }

        //
        // GET: /Store/
        public async Task<IActionResult> Index()
        {
            var genres = await DbContext.Genres.ToListAsync();

            return View(genres);
        }

        //
        // GET: /Store/Browse?genre=Disco
        public async Task<IActionResult> Browse(string genre)
        {
            // Retrieve Genre genre and its Associated associated Albums albums from database
            var genreModel = await DbContext.Genres
                .Include(g => g.Albums)
                .Where(g => g.Name == genre)
                .FirstOrDefaultAsync();

            if (genreModel == null)
            {
                return NotFound();
            }

            return View(genreModel);
        }

        public async Task<IActionResult> Details(
            [FromServices] IMemoryCache cache,
            int id)
        {
            var cacheKey = string.Format("album_{0}", id);
            Album album;
            if (!cache.TryGetValue(cacheKey, out album))
            {
                album = await DbContext.Albums
                                .Where(a => a.AlbumId == id)
                                .Include(a => a.Artist)
                                .Include(a => a.Genre)
                                .FirstOrDefaultAsync();

                if (album != null)
                {
                    if (_appSettings.CacheDbResults)
                    {
                        //Remove it from cache if not retrieved in last 10 minutes
                        cache.Set(
                            cacheKey,
                            album,
                            new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10)));
                    }
                }
            }

            if (album == null)
            {
                return NotFound();
            }

            return View(album);
        }
        //
        // GET: /Store/
        public async Task<String[]> Genre([FromServices] IMemoryCache cache)
        {
            var cacheKey = string.Format("genre");
            string[] genre;
            if (!cache.TryGetValue(cacheKey, out genre))
            {
                var genres = await DbContext.Genres.ToArrayAsync();
                if (genres != null)
                {
                    genre = new String[genres.Count()];
                    int index = 0;
                    foreach (Models.Genre obj in genres)
                    {
                        genre[index] = obj.Name;
                        index++;
                    }
                    //Remove it from cache if not retrieved in last 10 minutes
                    cache.Set(
                        cacheKey,
                        genre,
                        new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10)));
                }
                return genre;
            }
            return genre;
        }
        //
        // GET: /Store/GetAlbum?genre=Jazz
        public async Task<String[]> GetAlbum(string genre, [FromServices] IMemoryCache cache)
        {
            var cacheKey = string.Format("genre_{0}", genre);
            String[] albums;
            if (!cache.TryGetValue(cacheKey, out albums))
            {
                var genreModel = await DbContext.Genres
               .Include(g => g.Albums)
               .Where(g => g.Name == genre)
               .FirstOrDefaultAsync();

                if (genreModel != null)
                {
                    albums = new String[genreModel.Albums.Count()];
                    int index = 0;
                    foreach (Models.Album obj in genreModel.Albums)
                    {
                        albums[index] = obj.AlbumId.ToString();
                        index++;
                    }
                }

                if (albums != null)
                {
                    //Remove it from cache if not retrieved in last 10 minutes
                    cache.Set(
                        cacheKey,
                        albums,
                        new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(10)));
                }
                return albums;
            }
            else
            {
                return albums;
            }
        }
        //
        // GET: /Store/GetAlbumsCount
        public async Task<int> GetAlbumsCount()
        {
            var albumModel = await DbContext.Albums.ToArrayAsync();
            int albumsCount = 0;

            if (albumModel != null)
            {
                albumsCount = albumModel.Count();
            }
            return albumsCount;
        }
        //
        // GET: /Store/GetUsersCount/
        public async Task<int> GetUsersCount()
        {
            int usersCount;
            var userModel = await DbContext.Users.ToArrayAsync();
            usersCount = userModel.Count();
            return usersCount;
        }
    }
}