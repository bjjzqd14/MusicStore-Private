﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MSWClient;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            (new Clients()).RunTheBenchmark().Wait();
        }
    }
}
