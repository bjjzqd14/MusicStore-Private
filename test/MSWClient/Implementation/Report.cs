﻿/*using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace MusicStoreBenchmarks
{
    public class Report
    {
        static object syncObject = new object();
        public static string LogFile = String.Empty;
        static string ThreadInfoFile = String.Empty;

        internal static void InitLoggingAndTracers()
        {
            string fileName = String.Format("{0:MM-dd_HH-mm-ss}.log", DateTime.Now);

            if (!Directory.Exists(Config.ResultFolder))
            {
                try
                {
                    Directory.CreateDirectory(Config.ResultFolder);
                }
                catch (IOException e)
                {
                    Console.WriteLine("Exception in writing to file ", e.Message);
                }
            }

            LogFile = System.IO.Path.Combine(Config.ResultFolder, fileName);

            string threadInfoFile = String.Format("{0:MM-dd_HH-mm-ss}.threadInfo.csv", DateTime.Now);
            ThreadInfoFile = System.IO.Path.Combine(Config.ResultFolder, threadInfoFile);

            InitTraceListeners();
        }
        private static void InitTraceListeners()
        {
            Trace.Listeners.Clear();

            var textWriterTraceListener = new TextWriterTraceListener(Report.LogFile)
            {
                Name = "MSWServer",
                TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime
            };
            var consoleTraceListener = new ConsoleTraceListener(false) { TraceOutputOptions = TraceOptions.DateTime };
            Trace.Listeners.Add(textWriterTraceListener);
            Trace.Listeners.Add(consoleTraceListener);
            Trace.AutoFlush = true;
        }
        /// <summary>
        /// Log to file , generated in folder .../MusicStoreBenchmarksClients/Results 
        /// </summary>
        /// <param name="message"></param>
        internal static void Write(string message)
        {
            // lock (syncObject)
            {
                try
                {
                    System.IO.File.AppendAllText(LogFile, message);
                }
                catch (IOException e)
                {
                    Console.WriteLine("Exception in writing to file ", e.Message);
                }
            }
        }
        /// <summary>
        /// Log thread info to .tsv file , generated in folder .../MusicStoreBenchmarksClients/Results 
        /// </summary>
        /// <param name="threadInfo"></param>
        internal static void LogTransactionDetails(Clients.ThreadInfo threadInfo)
        {
            lock (syncObject)
            {
                try
                {
                    System.IO.File.AppendAllText(ThreadInfoFile, string.Format($"{threadInfo.number}\t{threadInfo.seqNo}\t{threadInfo.latency}"));
                }
                catch (IOException e)
                {
                    Console.WriteLine("Exception in writing to file ", e.Message);
                }
            }
        }
        /// <summary>
        /// Log thread info to .tsv file , generated in folder .../MusicStoreBenchmarksClients/Results 
        /// </summary>
        /// <param name="infoList"></param>
        internal static void LogTransactionDetails(ConcurrentBag<Clients.ThreadInfo> infoList)
        {
            try {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(ThreadInfoFile))
                {
                    foreach (Clients.ThreadInfo threadInfo in infoList)
                    {
                        // {threadInfo.startTime.Hour}:{threadInfo.startTime.Minute}:{threadInfo.startTime.Second}:{threadInfo.startTime.Millisecond},
                        file.WriteLine(string.Format($"{threadInfo.number},{threadInfo.seqNo},{threadInfo.latency}"));
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Exception in writing to file ", e.Message);
            }

        }
    }
}
*/