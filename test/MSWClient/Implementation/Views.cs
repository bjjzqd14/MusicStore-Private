using System;
using System.IO;
using System.IO.Pipes;
using System.Collections.Generic;

namespace MSWClient
{
    internal interface IView
    {
	
	void EndRampup();
        void BeginRunStatistics(int currentNumberOfUsers);
        void EndRunStatistics(RunStatistics runStatistics, RunValidation rv, DateTime measurementEndTime);
        void FinalStatistics(List<RunStatistics> runData);
        void RuntimeUpdate(RunStatistics rs);
    }

    internal class VisualizerView : IView
    {
	public static string LogFile = String.Empty;
        private int _visualizerRunningAttempts;
        private string _pipeName;
        public VisualizerView(string pipeName)
        {
            _visualizerRunningAttempts = 0;
            _pipeName = pipeName;
        }


	public void EndRampup(){}
        public void BeginRunStatistics(int _) {}
        public void EndRunStatistics(RunStatistics _1, RunValidation _2, DateTime _3) {}
        public void FinalStatistics(List<RunStatistics> runData)
        {            
            if (_visualizerRunningAttempts < 5)
            {
                SendRunDataToVisualizer("finished");
                foreach (RunStatistics data in runData)
                {
                    SendRunDataToVisualizer(data.ToVisualizerSummary());
                }
            }
        }
        
        public void SendRunDataToVisualizer(string measurements)
        {
            try
            {
                using (NamedPipeClientStream pipe = new NamedPipeClientStream(".", "MSWMusicStorePipes", PipeDirection.Out))
                {
                    using (StreamWriter stream = new StreamWriter(pipe))
                    {
                        pipe.Connect(2000);
                        stream.Write(measurements);
                    }
                    _visualizerRunningAttempts = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message} in SendRunDataToVisualizer");
                _visualizerRunningAttempts++;
                if (_visualizerRunningAttempts > 5)
                    Console.WriteLine($"Failed to connect to visualizer after 5 attempts. {ex.Message}");
            }
        }

        public void RuntimeUpdate(RunStatistics rs)
        {
            if (_visualizerRunningAttempts < 5)
            {
                SendRunDataToVisualizer(rs.ToVisualizer());
            }
        }
    }

    internal class ConsoleView : IView
    {
	public static string LogFile = String.Empty;
	string fileName = String.Format("{0:MM-dd_HH-mm-ss}.log", DateTime.Now);
        public void BeginRunStatistics(int currentNumberOfUsers)
        {
		if (!Directory.Exists(Config.ResultFolder))
            	{
               		try
                	{
                   		Directory.CreateDirectory(Config.ResultFolder);
                	}
                	catch (IOException e)
                	{
                    		Console.WriteLine("Exception in writing to file ", e.Message);
                	}
            	}

		LogFile = Path.Combine(Config.ResultFolder, fileName);
	    	File.AppendAllText(LogFile, $"\n====== Music Store Client Report {DateTime.Now} ======\n");
		File.AppendAllText(LogFile, "\nInput parameters:\n");
		File.AppendAllText(LogFile, Config.GetConfigSummary());
		File.AppendAllText(LogFile, $"Starting Rampup for {currentNumberOfUsers} Users at {DateTime.Now}\n");
		File.AppendAllText(LogFile,RunStatistics.ToCSVHeader());

            Console.WriteLine("\n========== Music Store Client Report ===========\n");
            Console.WriteLine("\nInput parameters:\n");
            Console.WriteLine(Config.GetConfigSummary());
            Console.WriteLine($"\nStarting Rampup for {currentNumberOfUsers} Users at {DateTime.Now}");
            Console.WriteLine(RunStatistics.ToCSVHeader());
        }
	public void EndRampup()
	{
		File.AppendAllText(LogFile,$"\nRampup Ended, Starting Measurement at {DateTime.Now}");
 		Console.WriteLine($"Rampup Ended, Starting Measurement at {DateTime.Now}");
	}
        public void EndRunStatistics(RunStatistics runStatistics, RunValidation rv, DateTime measurementEndTime)
        {
        File.AppendAllText(LogFile,$"\nEnding Measurement at : {measurementEndTime}\n");
		File.AppendAllText(LogFile, runStatistics.ToSummary());
		foreach (var pair in rv.SequenceCounts) File.AppendAllText(LogFile,$"  Sequence {pair.Key}: {pair.Value} <{Math.Round((double)pair.Value / (double)rv.OneX)}x>\n");
		File.AppendAllText(LogFile,$"\n  Validation: {(rv.Pass ? "PASS" : "FAIL")}\n");
		File.AppendAllText(LogFile,$"  Total Sequences: {rv.TotalNumberOfSequences}\n");
	     Console.WriteLine($"Ending Measurement at : {measurementEndTime}");
            Console.WriteLine(runStatistics.ToSummary());
            foreach (var pair in rv.SequenceCounts) Console.WriteLine($"  Sequence {pair.Key}: {pair.Value} <{Math.Round((double)pair.Value / (double)rv.OneX)}x>");
            Console.WriteLine($"\n  Validation: {(rv.Pass ? "PASS" : "FAIL")}");
            Console.WriteLine($"  Total Sequences: {rv.TotalNumberOfSequences}");
        }

        private void printFinalSummary(List<RunStatistics> data)
        {
            double maxThroughput = 0;
            int indexOfMaxThroughput = data.Count - 1;
            double aggregatedPercentileResponse = 0;

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].Throughput > maxThroughput)
                {
                    indexOfMaxThroughput = i;
                    maxThroughput = data[i].Throughput;
                }
            }
            for (int i = 0; i <= indexOfMaxThroughput; i++)
            {
                aggregatedPercentileResponse += data[i].ResponseTimeNinetyNinePercentile;
            }
            aggregatedPercentileResponse /= indexOfMaxThroughput + 1;

		File.AppendAllText(LogFile, $"\n================= Final Summary Report ====================\n");
		File.AppendAllText(LogFile, "Primary Metric\n");
		File.AppendAllText(LogFile, $" Maximum Throughput: {Math.Round(data[indexOfMaxThroughput].Throughput, 2)} (sequences/sec) @ {data[indexOfMaxThroughput].NumberOfUsers} Users\n");
		File.AppendAllText(LogFile, "\nSecondary Metric\n");
		File.AppendAllText(LogFile, $"  Aggregated Response Time: {Math.Round(aggregatedPercentileResponse, 2)} (milliseconds/sequence)\n");
		File.AppendAllText(LogFile, $"    Average of 99 percentile for load level until max throughput\n");
		File.AppendAllText(LogFile, $"\n===========================================================\n");
		File.AppendAllText(LogFile, $"\t\tAvg\tMin\tMax\t99Pct.\tStd.Dev.\n");
		File.AppendAllText(LogFile, "Users\tThroughput\tLatency\tLatency\tLatency\tLatency\tLatency\n");

            Console.WriteLine($"\n================= Final Summary Report ====================");
            Console.WriteLine("Primary Metric");
            Console.WriteLine($" Maximum Throughput: {Math.Round(data[indexOfMaxThroughput].Throughput, 2)} (sequences/sec) @ {data[indexOfMaxThroughput].NumberOfUsers} Users");
            Console.WriteLine("\nSecondary Metric");
            Console.WriteLine($"  Aggregated Response Time: {Math.Round(aggregatedPercentileResponse, 2)} (milliseconds/sequence)");
            Console.WriteLine($"    Average of 99 percentile for load level until max throughput");
            Console.WriteLine($"\n===========================================================");
            Console.WriteLine($"\t\t\tAvg\tMin\tMax\t99Pct.\tStd.Dev.");
            Console.WriteLine("Users\tThroughput\tLatency\tLatency\tLatency\tLatency\tLatency");
            for (int i = 0; i < data.Count; i++)
            {
                if (i == indexOfMaxThroughput)
                {
		    File.AppendAllText(LogFile, "*");
                    Console.Write("*");
                }
		File.AppendAllText(LogFile,data[i].ToCSVSummary());
		File.AppendAllText(LogFile,"\n");
                Console.WriteLine(data[i].ToCSVSummary());
            }
		File.AppendAllText(LogFile, $"\n===========================================================\n");
            Console.WriteLine($"\n===========================================================");
        }

        public void FinalStatistics(List<RunStatistics> runData)
        {
	    File.AppendAllText(LogFile,"=============== Program Execution Complete ================\n");
            Console.WriteLine("=============== Program Execution Complete ================");
            
            if (Config.Mode == "Auto" && runData.Count != 1)
            {
                printFinalSummary(runData);
            }
        }

        public void RuntimeUpdate(RunStatistics rs)
        {
	        File.AppendAllText(LogFile, "\n");
	        File.AppendAllText(LogFile, rs.ToCSV());
            Console.WriteLine(rs.ToCSV());
        }
    }

    class Views : IView
    {
        List<IView> _views;

        public Views()
        {
            _views = new List<IView>();
            if (Config.VisualizerNamedPipe != null)
            {
                _views.Add(new VisualizerView(Config.VisualizerNamedPipe));
            }
            if (Config.ConsoleOutput == true)
            {
                _views.Add(new ConsoleView());
            }
        }

        public void BeginRunStatistics(int currentNumberOfUsers)
        {
            foreach (IView view in _views)
            {
                view.BeginRunStatistics(currentNumberOfUsers);
            }
        }

        public void EndRunStatistics(RunStatistics runStatistics, RunValidation rv, DateTime measurementEndTime)
        {
            foreach (IView view in _views)
            {
                view.EndRunStatistics(runStatistics, rv, measurementEndTime);
            }
        }

        public void FinalStatistics(List<RunStatistics> runData)
        {
            foreach (IView view in _views)
            {
                view.FinalStatistics(runData);
            }
        }

        public void RuntimeUpdate(RunStatistics rs)
        {
            foreach (IView view in _views)
            {
                view.RuntimeUpdate(rs);
            }
        }

	public void EndRampup()
	{
	    foreach (IView view in _views)
            {
                view.EndRampup();
            }
	}
    }
}