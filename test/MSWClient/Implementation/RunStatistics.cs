using System;
using System.Collections.Generic;
using System.Linq;

namespace MSWClient {
    class TransactionStatistics
    {
        private double _responseTimeNinetyNinePercentile;
        public double ResponseTimeNinetyNinePercentile { get { return _responseTimeNinetyNinePercentile; } }
        private double _responseTimeMin;
        public double ResponseTimeMin { get { return _responseTimeMin; } }
        private double _responseTimeMax;
        public double ResponseTimeMax { get { return _responseTimeMax; } }
        private double _responseTimeAverage;
        public double ResponseTimeAverage { get { return _responseTimeAverage; } }
        private double _responseTimeStandardDeviation;
        public double ResponseTimeStandardDeviation { get { return _responseTimeStandardDeviation; } }

        public TransactionStatistics(List<TransactionData> transactionInfo)
        {
            if (transactionInfo.Count > 0)
            {
                _responseTimeMin = transactionInfo.Min((r => r.Latency));
                _responseTimeMax = transactionInfo.Max((r => r.Latency)); 
                _responseTimeAverage = transactionInfo.Average((r => r.Latency));
                _responseTimeStandardDeviation = Math.Sqrt(RunStatistics.SumOfSquares(transactionInfo, _responseTimeAverage) / transactionInfo.Count);
                _responseTimeNinetyNinePercentile = RunStatistics.Percentile(transactionInfo);
            }
        }

        public string ToSummary(char separator = '\t')
        {
            return String.Format($"{_responseTimeMin}{separator}{Math.Round(_responseTimeAverage, 2)}{separator}{_responseTimeMax}{separator}{_responseTimeNinetyNinePercentile}{separator}{Math.Round(_responseTimeStandardDeviation, 2)}");
        }
    }

    class SequenceStatistics
    {
        private double _responseTimeNinetyNinePercentile;
        public double ResponseTimeNinetyNinePercentile { get { return _responseTimeNinetyNinePercentile; } }
        private double _responseTimeMin;
        public double ResponseTimeMin { get { return _responseTimeMin; } }
        private double _responseTimeMax;
        public double ResponseTimeMax { get { return _responseTimeMax; } }
        private double _responseTimeAverage;
        public double ResponseTimeAverage { get { return _responseTimeAverage; } }
        private double _responseTimeStandardDeviation;
        public double ResponseTimeStandardDeviation { get { return _responseTimeStandardDeviation; } }
        private Dictionary<TransactionType, TransactionStatistics> _typeToTransactionRunData;
        public Dictionary<TransactionType, TransactionStatistics> TransactionStatistics { get { return _typeToTransactionRunData; } }
        
        public SequenceStatistics(List<SequenceData> sequenceData)
        {
            if (0 < sequenceData.Count)
            {
                _typeToTransactionRunData = new Dictionary<TransactionType, TransactionStatistics>();
                _responseTimeMin = sequenceData.Min((r => r.Latency));
                _responseTimeMax = sequenceData.Max((r => r.Latency)); 
                _responseTimeAverage = sequenceData.Average((r => r.Latency));
                _responseTimeStandardDeviation = Math.Sqrt(RunStatistics.SumOfSquares(sequenceData, _responseTimeAverage) / sequenceData.Count);
                _responseTimeNinetyNinePercentile = RunStatistics.Percentile(sequenceData);

                Dictionary<TransactionType, List<TransactionData>> txnData = new Dictionary<TransactionType, List<TransactionData>>();
                foreach (SequenceData sd in sequenceData)
                {
                    sd.AddTransactionDataTo(txnData);
                }

                foreach (TransactionType txn in Enum.GetValues(typeof(TransactionType)))
                {
                    if (txnData.ContainsKey(txn)) _typeToTransactionRunData[txn] = new TransactionStatistics(txnData[txn]);
                }
            }
        }
        
        public static string ToCSVHeader(char separator = '\t')
        {
            return String.Format($"Seq#{separator}Min{separator}Avg{separator}Max{separator}99%{separator}Stdev");
        }
        
        public string ToCSV(char separator = '\t')
        {
            return (String.Format($"{_responseTimeMin}{separator}{Math.Round(_responseTimeAverage, 2)}{separator}{_responseTimeMax}{separator}{_responseTimeNinetyNinePercentile}{separator}{Math.Round(_responseTimeStandardDeviation, 2)}"));
        }
    }

    class RunStatistics
    {
        private int _numberOfUsers;
        public int NumberOfUsers { get { return _numberOfUsers; } }
        private double _throughput;
        public double Throughput { get { return _throughput; } }
        public double _responseTimeNinetyNinePercentile;
        public double ResponseTimeNinetyNinePercentile { get { return _responseTimeNinetyNinePercentile; } }
        private double _responseTimeMin;
        public double ResponseTimeMin { get { return _responseTimeMin; } }
        private double _responseTimeMax;
        public double ResponseTimeMax { get { return _responseTimeMax; } }
        private double _responseTimeAverage;
        public double ResponseTimeAverage { get { return _responseTimeAverage; } }
        private double _responseTimeStandardDeviation;
        public double ResponseTimeStandardDeviation { get { return _responseTimeStandardDeviation; } }
        
        private SequenceStatistics [] _sequenceStatistics;
        public SequenceStatistics [] GetSequenceStatistics() { return _sequenceStatistics; }
        
        public static double SumOfSquares(List<ILatency> threadInfo, double mean)
        {
            double sum = 0.0;
            foreach (ILatency ti in threadInfo)
            {
                sum += (ti.Latency - mean) * (ti.Latency - mean);
            }
            return sum;
        }
        
        // Just converts List<ThreadInfo> to List<ILatency>
        public static double SumOfSquares(List<SequenceData> threadInfo, double mean)
        {
            List<ILatency> il = new List<ILatency>();
            foreach (SequenceData ti in threadInfo)
            {
                il.Add(ti);
            }
            return SumOfSquares(il, mean);
        }
        
        // Just converts List<SequenceInfo> to List<ILatency>
        public static double SumOfSquares(List<TransactionData> sequenceInfo, double mean)
        {
            List<ILatency> il = new List<ILatency>();
            foreach (TransactionData ti in sequenceInfo)
            {
                il.Add(ti);
            }
            return SumOfSquares(il, mean);
        }
        
        // Percentile calcluation for array of numbers
        // referring to algorithm at http://www.dummies.com/how-to/content/how-to-calculate-percentiles-in-statistics.html
        //
        public static double Percentile(List<ILatency> bag, double percentile = 0.99)
        {
            double latencyPercentile = 0.0;
            List<ILatency> sortedList = bag.OrderBy((r => r.Latency)).ToList();
            double index = (percentile * (sortedList.Count));

            if (index % 1 == 0)
            {
                // index is whole number
                // take the average of next two values
                int i = Convert.ToInt32(index);
                latencyPercentile = ((double)sortedList[i - 1].Latency + (double)sortedList[i].Latency) / 2.0;
            }
            else
            {
                // Math.Round(2.5) return 2. we want to 3
                // http://stackoverflow.com/questions/977796/why-does-math-round2-5-return-2-instead-of-3-in-c
                //
                int i = Convert.ToInt32(Math.Round(index, 0, MidpointRounding.AwayFromZero));
                latencyPercentile = sortedList[i - 1].Latency;
            }
            return latencyPercentile;
        }
        
        // Just converts List<ThreadInfo> to List<ILatency>
        public static double Percentile(List<SequenceData> bag, double percentile = 0.99)
        {
            List<ILatency> il = new List<ILatency>();
            foreach (SequenceData ti in bag)
            {
                il.Add(ti);
            }
            return Percentile(il, percentile);
        }

        // Just converts List<SequenceInfo> to List<ILatency>
        public static double Percentile(List<TransactionData> bag, double percentile = 0.99)
        {
            List<ILatency> il = new List<ILatency>();
            foreach (TransactionData si in bag)
            {
                il.Add(si);
            }
            return Percentile(il, percentile);
        }
        
        public RunStatistics(int numberOfUsers, int numSequences, List<IntervalStatistics> intervalStatisticsList, int rampupCounter, double duration)
        {
            _numberOfUsers = numberOfUsers;

            double avg = 0, min = 0, max = 0, stdDev = 0, throughput = 0, ninetyNinePercentile = 0;

            for(int i=0; i<intervalStatisticsList.Count; i++)
            {
                throughput += intervalStatisticsList[i].throughput;
                min += intervalStatisticsList[i].min;
                max += intervalStatisticsList[i].max;
                avg += intervalStatisticsList[i].avg;
                ninetyNinePercentile += intervalStatisticsList[i].ninetyNinePercentile;
                stdDev += intervalStatisticsList[i].stdDev;
            }

            throughput = throughput / intervalStatisticsList.Count;
            min = min / intervalStatisticsList.Count;
            max = max / intervalStatisticsList.Count;
            avg = avg / intervalStatisticsList.Count;
            ninetyNinePercentile = ninetyNinePercentile / intervalStatisticsList.Count;
            stdDev = stdDev / intervalStatisticsList.Count;

            _responseTimeAverage = avg;
            _responseTimeMin = min;
            _responseTimeMax = max;
            _responseTimeStandardDeviation = stdDev;
            _throughput = throughput;
            _responseTimeNinetyNinePercentile = ninetyNinePercentile;

        }

        
        public RunStatistics(int numberOfUsers, int numSequences, List<SequenceData> sequenceData, int rampupCounter, double duration)
        {
            _numberOfUsers = numberOfUsers;
            _sequenceStatistics = new SequenceStatistics[numSequences];
            _responseTimeAverage = sequenceData.GetRange(rampupCounter, sequenceData.Count - rampupCounter).Average((r => r.Latency));
            _responseTimeMin = sequenceData.GetRange(rampupCounter, sequenceData.Count - rampupCounter).Min((r => r.Latency));
            _responseTimeMax = sequenceData.GetRange(rampupCounter, sequenceData.Count - rampupCounter).Max((r => r.Latency));
            _responseTimeStandardDeviation = Math.Sqrt(SumOfSquares(sequenceData, _responseTimeAverage) / sequenceData.Count);
            _throughput = (sequenceData.Count - rampupCounter) / duration;
            _responseTimeNinetyNinePercentile = Percentile(sequenceData);
            for (int i = 0; i < numSequences; i++)
            {
                _sequenceStatistics[i] = new SequenceStatistics(sequenceData.Where((s => s.SeqNo == i)).ToList());
            }
        }

        public static string ToCSVHeader(char separator='\t')
        {
            return String.Format($"Users{separator}seq/s{separator}Min{separator}Mean{separator}99%{separator}StdDev{separator}Max");
        }
        
        public string ToCSV(char separator='\t')
        {
            return String.Format($"{_numberOfUsers}{separator}{Math.Round(_throughput, 2)}{separator}{_responseTimeMin}{separator}{Math.Round(_responseTimeAverage, 2)}{separator}{_responseTimeNinetyNinePercentile}{separator}{Math.Round(_responseTimeStandardDeviation, 2)}{separator}{_responseTimeMax}");
        }
        
        public string ToCSVSummary(char separator='\t')
        {
            return String.Format($"{_numberOfUsers}{separator}{Math.Round(_throughput, 2)} seq/sec{separator}{Math.Round(_responseTimeAverage, 0)}ms{separator}{Math.Round(_responseTimeMin, 0)}ms{separator}{Math.Round(_responseTimeMax, 0)}ms{separator}{Math.Round(_responseTimeNinetyNinePercentile, 0)}ms\t{Math.Round(_responseTimeStandardDeviation, 0)}ms");
        }
        
        public string ToVisualizer(char separator='\t')
        {
            return String.Format($"#{_numberOfUsers}{separator}{Math.Round(_throughput, 2)}{separator}{separator}{separator}{_responseTimeMin}{separator}{Math.Round(_responseTimeAverage, 2)}{separator}{_responseTimeNinetyNinePercentile}{separator}{Math.Round(_responseTimeStandardDeviation, 2)}{separator}{_responseTimeMax}");
        }
        
        public string ToVisualizerSummary()
        {
            return String.Format($"+{_numberOfUsers}\t{Math.Round(_throughput, 2)}\t{Math.Round(_responseTimeAverage, 0)}\t{Math.Round(_responseTimeMin, 0)}\t{Math.Round(_responseTimeMax, 0)}\t{Math.Round(_responseTimeNinetyNinePercentile, 0)}\t{Math.Round(_responseTimeStandardDeviation, 0)}");
        }

        public string ToSummary()
        {
            String retval = String.Format($"\n================ Results @ {NumberOfUsers} Users ======================\n");        
            retval += String.Format($"  Throughput: {Math.Round(Throughput, 2)} (sequences/sec) @ {NumberOfUsers} Users\n");
            retval += String.Format($"  Response time (milliseconds/sequence)\n");
	    retval += String.Format($"    99 Percentile: {ResponseTimeNinetyNinePercentile}\n");
            retval += String.Format($"    Min: {ResponseTimeMin}\n");
            retval += String.Format($"    Max: {ResponseTimeMax}\n");
            retval += String.Format($"    Average: {Math.Round(ResponseTimeAverage, 2)}\n");
            retval += String.Format($"    Standard Deviation: {Math.Round(ResponseTimeStandardDeviation, 2)}\n");
           /* retval += String.Format($"\n========= Detailed Sequence Response Times (ms) =========\n");  
	       retval += String.Format(SequenceStatistics.ToCSVHeader());
          /*  for (int i = 0; i < GetSequenceStatistics().Count(); i++)
            {
                retval += String.Format($"\n{i}\t" + GetSequenceStatistics()[i].ToCSV() + '\n');
                foreach (TransactionType txn in Enum.GetValues(typeof(TransactionType)))
                {
                    if (GetSequenceStatistics()[i].TransactionStatistics.ContainsKey(txn)) 
                    {
                        retval += String.Format($"\n\t{txn}\n\t");
                        retval += GetSequenceStatistics()[i].TransactionStatistics[txn].ToSummary();
                    }
                }
            }
         */   retval += String.Format($"\n===========================================================\n");
            return retval;
        }

        private RunStatistics() {} // Always require initialization arguments
    }
}
