using System;
using System.Collections.Generic;
using System.Linq;

namespace MSWClient
{
    class RunValidation
    {
        private bool _pass;
        public bool Pass { get { return _pass; } }

        private int _totalNumberOfSequences;
        public int TotalNumberOfSequences { get { return _totalNumberOfSequences; } }

        private Dictionary<int, int> _totalSeq;
        public Dictionary<int, int> SequenceCounts { get { return _totalSeq; } }

        private int _oneX;
        public int OneX { get { return _oneX; } }

        //public RunValidation(List<SequenceData> _threadInfoBag)
        public RunValidation(SequenceDistributionStatistics sds)
        {

            _totalSeq = sds.sequenceCount;
            _pass = true;
            _totalNumberOfSequences = sds.numSequences;
            
           /* if (_threadInfoBag.Count > 0)
            {
                for (int i = 0; i < Config.TotalNoOfSequences; i++)
                {
                    _totalSeq.Add(i, 0);
                }
                foreach (SequenceData ti in _threadInfoBag)
                {
                    if (_totalSeq.ContainsKey(ti.SeqNo))
                        _totalSeq[ti.SeqNo]++;
                }
                */
                _oneX = _totalSeq[0] / Config.SequenceDistribution[0];
                _totalNumberOfSequences = _totalSeq.Values.Sum();
            
                const double percentileLowerLimit = 0.9;
                const double percentileUpperLimit = 1.2;

                foreach (var pair in _totalSeq)
                {
                    double d = (_oneX * (double)Config.SequenceDistribution[pair.Key]) / (double)pair.Value;
 
                    if (d < percentileLowerLimit || d > percentileUpperLimit)
                    {
                        _pass = false;
                    }
                }
            /*}
        */}
    }
}