using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

public class Config
{
    // Define an extension method in a non-nested static class.
    private static int users = 0, albums = 0, rampupTime = 0,
                        measurementTime = 0, runtimeThroughputTime = 0, maxNumberOfUsers = 0, incrementalUsers = 0, runsAfterMax = 0;
    private static List<int> seqDistribution;
    private static List<List<TransactionType>> sequence;
    private static bool? verbose = null;
    private static bool? _consoleOutput = null;
    private static string threadInfologFile = String.Empty;
    private static string mode = String.Empty;
    private static string applicationBaseUrl = String.Empty;
    private static string musicStoreDbName = String.Empty;
    private static string _adminUsername = null, _adminPassword = null;
    // private static IConfigurationRoot config = null;
    private static string _visualizer = null;
    private static IConfiguration config=null;

    private static void ReadConfigFile()
    {
        IConfigurationBuilder builder = new ConfigurationBuilder().AddJsonFile("config.json");
        config = builder.Build();
    }
    private static string GetString(string key)
    {
        if (config == null)
            ReadConfigFile();
        return config[key];
    }

    public static string GetConfigSummary()
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine($" Mode: {mode}");
        sb.AppendLine($" Number of Albums added: {albums}");
        sb.AppendLine($" Number of Guests: {users}");
        sb.AppendLine($" Ramp Up Time: {RampUpTime}");
        sb.AppendLine($" Measurement Time: {MeasurementTime}");
        sb.AppendLine($" ResultFolder: {ResultFolder}");
        sb.AppendLine($" Accessing Server Application at {applicationBaseUrl}");
        sb.AppendLine($" Runtime Throughput Timer Interval: {RuntimeThroughputTime}");
        sb.AppendLine($" Maximum Number of Users (Auto Mode): {MaxNumberOfUsers}");
        sb.AppendLine($" User Increment Per Run (Auto Mode): {IncrementalUsers}");
        sb.AppendLine($" Number of Runs Past Peak Throughput (Auto Mode): {RunsAfterMax}");	
	sb.AppendLine($" Number of Sequences: {TotalNoOfSequences}");
	sb.AppendLine($" Sequence Distribution: {String.Join(", ", SequenceDistribution.ToArray())}");
	
        //sb.AppendLine($" Visualizer Named Pipe: {VisualizerNamedPipe}");

        return sb.ToString();
    }
    public static int Guests
    {
        get
        {
            if (users == 0)
            {
                users = Convert.ToInt32(GetString("DefaultNoOfUsers"));                 
            }
            return users;
        }
    }
    public static int Albums
    {
        get
        {
            if (albums == 0)
            {
                albums = Convert.ToInt32(GetString("DefaultNoOfAlbums"));
            }
            return albums;
        }
    }
    public static int RampUpTime
    {
        get
        {
            if (rampupTime == 0)
            {
                rampupTime = Convert.ToInt32(GetString("RampUpTime"));
            }
            return rampupTime;
        }
    }
    public static int MeasurementTime
    {
        get
        {
            if (measurementTime == 0)
            {
                measurementTime = Convert.ToInt32(GetString("MeasurementTime"));
            }
            return measurementTime;
        }
    }
    public static List<int> SequenceDistribution
    {
        get
        {
            if (seqDistribution == null)
            {
                String seqDist = GetString("SequenceDistribution");

                String[] dist = seqDist.Split(',');
                seqDistribution = new List<int>();
                for (int i = 0; i < dist.Length; i++)
                {
                    int d = Int32.Parse(dist[i].Trim());
                    seqDistribution.Add(d);
                }
            }
            return seqDistribution;
        }
    }

    private static string sequences;

    public static List<List<TransactionType>> GetAllSequences
    {
        
        get
        {
            if (sequence == null)
            {
                sequence = new List<List<TransactionType>>();
                for (int i = 0; i < SequenceDistribution.Count; i++)
                {
                    String seq = GetString("Sequence" + $"{i}");
                    String[] seqDetails = seq.Split(',');

                    List<TransactionType> transactionList = new List<TransactionType>();
                    foreach (string token in seqDetails)
                    {
                        TransactionType txnType = TransactionType.NotDefined;
                        string strTransaction = token.Trim().ToLower();

                        if (String.Compare(strTransaction, TransactionType.login.ToString(), true) == 0)
                            txnType = TransactionType.login;
                        else if (String.Compare(strTransaction, TransactionType.homepage.ToString(), true) == 0)
                            txnType = TransactionType.homepage;
                        else if (String.Compare(strTransaction, TransactionType.logout.ToString(), true) == 0)
                            txnType = TransactionType.logout;
                        else if (String.Compare(strTransaction, TransactionType.search.ToString(), true) == 0)
                            txnType = TransactionType.search;
                        else if (String.Compare(strTransaction, TransactionType.searchAndBuy.ToString(), true) == 0)
                            txnType = TransactionType.searchAndBuy;
                        else if (String.Compare(strTransaction, TransactionType.searchAndAddToCart.ToString(), true) == 0)
                            txnType = TransactionType.searchAndAddToCart;

                        transactionList.Add(txnType);
                    }
                    sequences +=  String.Format($"  Sequence {i}: {seq}\n");
                    sequence.Add(transactionList);
                }
            }
            return sequence;
        }
    }
    public static bool? Verbose
    {
        get
        {
            if (verbose == null)
            {
                verbose = Convert.ToBoolean(GetString("Verbose"));
                if (verbose == null)
                    verbose = true;
            }
            return verbose;
        }
    }

    public static bool? ConsoleOutput
    {
        get
        {
            if (_consoleOutput == null)
            {
                _consoleOutput = Convert.ToBoolean(GetString("ConsoleOutput"));
                if (null == _consoleOutput) _consoleOutput = true;
            }
            return _consoleOutput;
        }
    }

    public static int TotalNoOfSequences
    {
        get
        {
            return SequenceDistribution.Count;
        }
    }
    public static string ResultFolder
    {
        get
        {
            if (string.IsNullOrEmpty(threadInfologFile))
            {
                threadInfologFile = GetString("ResultFolder");                    
            }
            return threadInfologFile;
        }
    }
    public static string ApplicationBaseUrl
    {
        get
        {
            if (String.IsNullOrEmpty(applicationBaseUrl))
            {
                applicationBaseUrl = GetString("applicationBaseUrl");                    
            }
            return applicationBaseUrl;
        }
    }
    public static int RuntimeThroughputTime
    {
        get
        {
            if (runtimeThroughputTime == 0)
            {
                runtimeThroughputTime = Convert.ToInt32(GetString("RuntimeThroughputTime"));                    
            }
            return runtimeThroughputTime;
        }
    }
    public static string Mode
    {
        get
        {
            if (String.IsNullOrEmpty(mode))
            {
                mode = GetString("Mode");                    
            }
            return mode;
        }
    }
    public static int MaxNumberOfUsers
    {
        get
        {
            if (maxNumberOfUsers == 0)
            {
                maxNumberOfUsers = Convert.ToInt32(GetString("MaxNumberOfUsers"));                    
            }
            return maxNumberOfUsers;
        }
    }
    public static int IncrementalUsers
    {
        get
        {
            if (incrementalUsers == 0)
            {
                incrementalUsers = Convert.ToInt32(GetString("IncrementalUsers"));                    
            }
            return incrementalUsers;
        }
    }
    public static int RunsAfterMax
    {
        get
        {
            if (runsAfterMax == 0)
            {
                runsAfterMax = Convert.ToInt32(GetString("RunsAfterMax"));
            }
            return runsAfterMax;
        }
    }
    public static string VisualizerNamedPipe
    {
        get
        {
            if (_visualizer == null)
            {
                _visualizer = GetString("VisualizerNamedPipe");
            }
            return _visualizer;
        }
    }

    public static string DefaultAdminUsername
    {
        get
        {
            if (null == _adminUsername)
            {
                _adminUsername = GetString("DefaultAdminUsername");
            }
            return _adminUsername;
        }
    }

    public static string DefaultAdminPassword
    {
        get
        {
            if (null == _adminPassword)
            {
                _adminPassword = GetString("DefaultAdminPassword");
            }
            return _adminPassword;
        }
    }
}
