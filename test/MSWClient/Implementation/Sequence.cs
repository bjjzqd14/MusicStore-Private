using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace MSWClient
{
    /// <summary>
    /// Sequence is the type of operations that any user could perform
    ///For eg: user can login to website , shop for albums , and logout Or
    ///user can simply login and logout and without shopping
    /// </summary>

    public class Sequence
    {
        private string email, password;
        private Validator _validator;
        private static HttpResponseMessage _response;

        public void Init(int ThreadNo)
        {
            var cookieContainer = new CookieContainer();
            var baseAddress = new Uri(Config.ApplicationBaseUrl);
            var httpClientHandler = new HttpClientHandler() { CookieContainer = cookieContainer };
            var httpClient = new HttpClient(httpClientHandler) {
                BaseAddress = baseAddress,
                Timeout = new System.TimeSpan(0, 0, 30) 
            };
            cookieContainer.Add(baseAddress, new Cookie("CookieName", "cookie_value" + ThreadNo));
            email = "user" + ThreadNo + "@test.com";//"Administrator@test.com";
            password = "Password@" + ThreadNo; //"YouShouldChangeThisPassword1!";
            _validator = new Validator(httpClient, httpClientHandler);
        }

        public async Task Execute(TransactionType txn)
        {
            switch (txn)
            {
                case TransactionType.homepage:
                    await _validator.VerifyHomePage(_response);
                    break;
                case TransactionType.login:
                    await _validator.SignInWithUser(email, password);
                    break;
                case TransactionType.search:
                    await _validator.SearchAlbums();
                    break;
                case TransactionType.searchAndAddToCart:
                    await _validator.SearchAlbumsAndAddtoCart();
                    break;
                case TransactionType.searchAndBuy:
                    await _validator.SearchAlbumsAddtoCartAndBuy();
                    break;
                case TransactionType.logout:
                    await _validator.SignOutUser(email);
                    break;
                case TransactionType.NotDefined:
                    Console.WriteLine("Invalid Transaction Type");
                    break;
            }
        }
        
        public static void setResponse(HttpResponseMessage response)
        {
            _response = response;
        }
    }
}
