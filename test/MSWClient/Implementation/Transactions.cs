using System;
using System.Linq;
using System.Collections.Generic;

public enum TransactionType
{
    homepage,
    login,
    logout,
    search,
    searchAndAddToCart,
    searchAndBuy,
    NotDefined
}

public class Transactions
{
    Random rnd = new Random();
    List<int> seqInOrder = new List<int>();
    internal List<List<TransactionType>> allSequences = Config.GetAllSequences;
    internal List<int> workerDist = new List<int>();
    int ratioSum = Config.SequenceDistribution.Sum();

    public Transactions(List<int> ratios)
    {
        int sequence = 0;
        foreach (int ratio in ratios)
        {
            for (int j = 0; j < ratio; j++)
                seqInOrder.Add(sequence);
            sequence++;
        }
    }
    /// <summary>
    /// This function returns sequence occurrences in random order
    /// Example:
    /// sequences 0, 1, 2
    /// Sequence distribution: 4,3,1
    /// Execution of sequences in order: 0,0,0,0,1,1,1,2
    /// Execution of sequences in RANDOM order: 0,1,0,0,2,1,1,0
    /// </summary>
    /// <param name="ratios">Sequence distribution: 4,3,1</param>
    /// <returns>random list of sequences</returns>
    internal List<int> RandomSequence()
    {
        List<int> seqInRandom = new List<int>();
        var nums = Enumerable.Range(0, ratioSum).OrderBy(x => rnd.Next()).Take(ratioSum).ToList();

        foreach (int num in nums)
        {
            seqInRandom.Add(seqInOrder[num]);
        }
        return seqInRandom;
    }
}
