using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

using System.Linq;

namespace MSWClient
{
    public enum RunState
    {
        Uninitialized,
        Rampup,
        Running,
        Ended
    }

    public struct SequenceDistributionStatistics
    {
        public int numSequences;
        public Dictionary<int, int> sequenceCount;
    }

    public struct IntervalStatistics
    {
        public double throughput;
        public double min;
        public double max;
        public double avg;
        public double ninetyNinePercentile;
        public double stdDev;
    }

    interface ILatency
    {
        long Latency { get; }
    }
    
    public class TransactionData : ILatency
    {
        private long _latency;
        public long Latency { get { return _latency; } }
        private TransactionType _txn;
        public TransactionType Txn { get { return _txn; } }
        
        public TransactionData(long latency, TransactionType txn)
        {
            _latency = latency;
            _txn = txn;
        }

        public void AddTransactionInfoTo(Dictionary<TransactionType, List<TransactionData>> original)
        {
            if (!original.ContainsKey(_txn)) original[_txn] = new List<TransactionData>();
            original[_txn].Add(this);
        }
    }

    class SequenceData : ILatency
    {
        private List<TransactionData> _sequenceInfoList;
        private int _number; // thread index
        public int Number { get { return _number; } }
        private int _seqNo; // sequence number read from config file
        public int SeqNo { get { return _seqNo; } }
        
        public SequenceData(int number, int seqNo)
        {
            _number = number;
            _seqNo = seqNo;
            _sequenceInfoList = new List<TransactionData>();
        }
        
        public void Add(long latency, TransactionType txn)
        {
            lock (_sequenceInfoList)
            {
                _sequenceInfoList.Add(new TransactionData(latency, txn));
            }
        }

        public void AddTransactionDataTo(Dictionary<TransactionType, List<TransactionData>> original)
        {
            lock (_sequenceInfoList)
            {
                foreach (TransactionData td in _sequenceInfoList)
                {
                    if (!original.ContainsKey(td.Txn)) 
                    {
                        original[td.Txn] = new List<TransactionData>();
                    }
                    original[td.Txn].Add(td);
                }
            }
        }
        public long Latency
        {
            get
            {
                long sum = 0;
                lock (_sequenceInfoList)
                {
                    foreach (TransactionData td in _sequenceInfoList)
                    {
                        sum += td.Latency;
                    }
                }
                return sum;
            }
        }
    }

    class RunData
    {
        // Threadsafe Bag
        private ConcurrentQueue<SequenceData> _threadInfoBag;
        private ConcurrentQueue<SequenceData> _shortTermBag;
        private List<IntervalStatistics> _intervalStatisticsList;
        private RunState runState = RunState.Uninitialized;
        private SequenceDistributionStatistics _sequenceDistributionStatistics;
        int _rampupCounter = 0;

        public void SetRunState(RunState inRunState)
        {
            runState = inRunState;
        }

        public RunState GetRunState()
        {
            return runState;
        }

        public RunData() 
        {
            _threadInfoBag = new ConcurrentQueue<SequenceData>();
            _shortTermBag = new ConcurrentQueue<SequenceData>();
            _intervalStatisticsList = new List<IntervalStatistics>();
            _sequenceDistributionStatistics = new SequenceDistributionStatistics();
            _sequenceDistributionStatistics.sequenceCount = new Dictionary<int, int>();
        }
        
        public void AddIntervalStatistics(double throughput, double min, double max, double avg, double ninetyNinePercentile, double stdDev)
        {
            IntervalStatistics intervalStats = new IntervalStatistics();

            intervalStats.throughput = throughput;
            intervalStats.min = min;
            intervalStats.max = max;
            intervalStats.ninetyNinePercentile = ninetyNinePercentile;
            intervalStats.stdDev = stdDev;
            intervalStats.avg = avg;

            _intervalStatisticsList.Add(intervalStats);
        }

        public SequenceDistributionStatistics getSequenceDistributionStatistics()
        {
            return _sequenceDistributionStatistics;
        }

        public SequenceData Add(int number, int seqNo)
        {
            SequenceData ti = new SequenceData(number, seqNo);
            lock (_shortTermBag) // Required for reassignment in RuntimeThroughput() and OnMeasurementStart().
            {
                _shortTermBag.Enqueue(ti);
            }

            _sequenceDistributionStatistics.numSequences++;

            if(_sequenceDistributionStatistics.sequenceCount.ContainsKey(seqNo))
            {
                _sequenceDistributionStatistics.sequenceCount[seqNo]++;
            }
            else
            {
                _sequenceDistributionStatistics.sequenceCount.Add(seqNo, 1);
            }

            //_threadInfoBag.Enqueue(ti);
            return ti;
        }
        
        public void OnMeasurementStart() 
        {
            //_rampupCounter = _threadInfoBag.Count;
            ConcurrentQueue<SequenceData> newQueue = new ConcurrentQueue<SequenceData>();
            lock (_shortTermBag) 
            {
                _shortTermBag = newQueue;
            }
        }

        public RunStatistics GetRunStatistics(int numberOfUsers, DateTime measurementEndTime, DateTime rampupEndTime)
        {
            return new RunStatistics(numberOfUsers, Config.TotalNoOfSequences, _intervalStatisticsList, _rampupCounter, (measurementEndTime - rampupEndTime).TotalSeconds);
        }

        public RunValidation Validate()
        {
            //return new RunValidation();
            return new RunValidation(_sequenceDistributionStatistics);
        }

        public RunStatistics RuntimeThroughput(DateTime T2, DateTime T1, int currentUsers, string new24HrTimePatter)
        {
            ConcurrentQueue<SequenceData> oldBag = _shortTermBag;
            ConcurrentQueue<SequenceData> newBag = new ConcurrentQueue<SequenceData>();
            lock (_shortTermBag) // wait for threads to exit _shortTermBag.Enqueue()
            {
                _shortTermBag = newBag;
            }
            if (oldBag.Count > 0)
            {
                return new RunStatistics(currentUsers, Config.TotalNoOfSequences, oldBag.ToList(), 0, (T2 - T1).TotalSeconds);
            }
            else
            {
                return null;
            }
        }
    }
}
