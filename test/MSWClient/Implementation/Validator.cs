using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

public partial class Validator
{
    private HttpClient _httpClient;
    private HttpClientHandler _httpClientHandler;
    private static List<string> genreList;

    public Validator(
        HttpClient httpClient,
        HttpClientHandler httpClientHandler)
    {
        _httpClient = httpClient;
        _httpClientHandler = httpClientHandler;
    }

    public async Task VerifyHomePage(
        HttpResponseMessage response,
        bool useNtlmAuthentication = false)
    {
        string responseContent = await response.Content.ReadAsStringAsync();

        if (response.StatusCode != HttpStatusCode.OK)
        {
            Console.WriteLine("Home page content : {0}", responseContent);
        }

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        ValidateLayoutPage(responseContent);
        Assert.Contains(PrefixBaseAddress("<a href=\"/{0}/Store/Details/"), responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains("<title>Home Page – ASP.NET MVC Music Store</title>", responseContent, StringComparison.OrdinalIgnoreCase);

        if (!useNtlmAuthentication)
        {
            //We don't display these for Ntlm
            Assert.Contains("Register", responseContent, StringComparison.OrdinalIgnoreCase);
            Assert.Contains("Login", responseContent, StringComparison.OrdinalIgnoreCase);
        }

        Assert.Contains("www.github.com/aspnet/MusicStore", responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains("/Images/home-showcase.png", responseContent, StringComparison.OrdinalIgnoreCase);
    }

    public async Task VerifyNtlmHomePage(HttpResponseMessage response)
    {
        await VerifyHomePage(response, useNtlmAuthentication: true);

        var homePageContent = await response.Content.ReadAsStringAsync();

        //Check if the user name appears in the page
        Assert.Contains(
            string.Format("{0}\\{1}", Environment.GetEnvironmentVariable("USERDOMAIN"), Environment.GetEnvironmentVariable("USERNAME")),
            homePageContent, StringComparison.OrdinalIgnoreCase);
    }

    public void ValidateLayoutPage(string responseContent)
    {
        Assert.Contains("ASP.NET MVC Music Store", responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains(PrefixBaseAddress("<li><a href=\"/{0}\">Home</a></li>"), responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains(PrefixBaseAddress("<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"/{0}/Store\">Store <b class=\"caret\"></b></a>"), responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains("<ul class=\"dropdown-menu\">", responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains("<li class=\"divider\"></li>", responseContent, StringComparison.OrdinalIgnoreCase);
    }

    public async Task VerifyStaticContentServed()
    {
        var response = await _httpClient.GetAsync("favicon.ico");
        await ThrowIfResponseStatusNotOk(response);
        Assert.NotNull(response.Headers.ETag);

        //Check if you receive a NotModified on sending an etag
        _httpClient.DefaultRequestHeaders.IfNoneMatch.Add(response.Headers.ETag);
        response = await _httpClient.GetAsync("favicon.ico");
        Assert.Equal(HttpStatusCode.NotModified, response.StatusCode);
        _httpClient.DefaultRequestHeaders.IfNoneMatch.Clear();

        response = await _httpClient.GetAsync("Content/bootstrap.css");
        await ThrowIfResponseStatusNotOk(response);
    }

    public async Task AccessStoreWithPermissions()
    {
        var response = await _httpClient.GetAsync("Admin/StoreManager/");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        Assert.Equal<string>(Config.ApplicationBaseUrl + "Admin/StoreManager/", response.RequestMessage.RequestUri.AbsoluteUri);
    }

    private string PrefixBaseAddress(string url)
    {
#if DNX451
        url = _deploymentResult.DeploymentParameters.ServerType == ServerType.IIS ?
            string.Format(url, new Uri(Config.ApplicationBaseUrl).Segments[1].TrimEnd('/')) :
            string.Format(url, string.Empty);
#else
        url = string.Format(url, string.Empty);
#endif

        return url.Replace("//", "/").Replace("%2F%2F", "%2F").Replace("%2F/", "%2F");
    }

    public async Task AccessStoreWithoutPermissions(string email = null)
    {
        var response = await _httpClient.GetAsync("Admin/StoreManager/");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        ValidateLayoutPage(responseContent);

        if (email == null)
        {
            Assert.Contains("<title>Log in – ASP.NET MVC Music Store</title>", responseContent, StringComparison.OrdinalIgnoreCase);
            Assert.Contains("<h4>Use a local account to log in.</h4>", responseContent, StringComparison.OrdinalIgnoreCase);
            Assert.Equal<string>(Config.ApplicationBaseUrl + PrefixBaseAddress("Account/Login?ReturnUrl=%2F{0}%2FAdmin%2FStoreManager%2F"), response.RequestMessage.RequestUri.AbsoluteUri);
        }
        else
        {
            Assert.Contains("<title>Access denied due to insufficient permissions – ASP.NET MVC Music Store</title>", responseContent, StringComparison.OrdinalIgnoreCase);
        }
    }

    public async Task RegisterUserWithNonMatchingPasswords()
    {
        var response = await _httpClient.GetAsync("Account/Register");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        ValidateLayoutPage(responseContent);

        var generatedEmail = Guid.NewGuid().ToString().Replace("-", string.Empty) + "@test.com";
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Email", generatedEmail),
                new KeyValuePair<string, string>("Password", "Password~1"),
                new KeyValuePair<string, string>("ConfirmPassword", "Password~2"),
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Account/Register")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Account/Register", content);
        responseContent = await response.Content.ReadAsStringAsync();
        // Assert.Null(_httpClientHandler.CookieContainer.GetCookies(new Uri(Config.ApplicationBaseUrl)).GetCookieWithName(".AspNet.Microsoft.AspNet.Identity.Application"));
        Assert.Contains("<div class=\"text-danger validation-summary-errors\" data-valmsg-summary=\"true\"><ul><li>The password and confirmation password do not match.</li>", responseContent, StringComparison.OrdinalIgnoreCase);
    }

    public async Task<string> RegisterValidUser()
    {
        var response = await _httpClient.GetAsync("Account/Register");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        ValidateLayoutPage(responseContent);

        var generatedEmail = Guid.NewGuid().ToString().Replace("-", string.Empty) + "@test.com";
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Email", generatedEmail),
                new KeyValuePair<string, string>("Password", "Password~1"),
                new KeyValuePair<string, string>("ConfirmPassword", "Password~1"),
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Account/Register")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Account/Register", content);
        responseContent = await response.Content.ReadAsStringAsync();

        //Account verification
        Assert.Equal<string>(Config.ApplicationBaseUrl + "Account/Register", response.RequestMessage.RequestUri.AbsoluteUri);
        Assert.Contains("For DEMO only: You can click this link to confirm the email:", responseContent, StringComparison.OrdinalIgnoreCase);
        var startIndex = responseContent.IndexOf("[[<a href=\"", 0) + "[[<a href=\"".Length;
        var endIndex = responseContent.IndexOf("\">link</a>]]", startIndex);
        var confirmUrl = responseContent.Substring(startIndex, endIndex - startIndex);
        confirmUrl = WebUtility.HtmlDecode(confirmUrl);
        response = await _httpClient.GetAsync(confirmUrl);
        await ThrowIfResponseStatusNotOk(response);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains("Thank you for confirming your email.", responseContent, StringComparison.OrdinalIgnoreCase);
        return generatedEmail;
    }

    public async Task RegisterExistingUser(string email)
    {
        var response = await _httpClient.GetAsync("Account/Register");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Email", email),
                new KeyValuePair<string, string>("Password", "Password~1"),
                new KeyValuePair<string, string>("ConfirmPassword", "Password~1"),
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Account/Register")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Account/Register", content);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains(string.Format("User name &#x27;{0}&#x27; is already taken.", email), responseContent, StringComparison.OrdinalIgnoreCase);
    }

    public async Task SignOutUser(string email)
    {
        var response = await _httpClient.GetAsync(string.Empty);
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        ValidateLayoutPage(responseContent);
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Account/LogOff")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Account/LogOff", content);
        responseContent = await response.Content.ReadAsStringAsync();

    }

    public async Task SignInWithInvalidPassword(string email, string invalidPassword)
    {
        var response = await _httpClient.GetAsync("Account/Login");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Email", email),
                new KeyValuePair<string, string>("Password", invalidPassword),
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Account/Login")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Account/Login", content);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains("<div class=\"text-danger validation-summary-errors\" data-valmsg-summary=\"true\"><ul><li>Invalid login attempt.</li>", responseContent, StringComparison.OrdinalIgnoreCase);
        //Verify cookie not sent
        // Assert.Null(_httpClientHandler.CookieContainer.GetCookies(new Uri(Config.ApplicationBaseUrl)).GetCookieWithName(".AspNet.Microsoft.AspNet.Identity.Application"));
    }

    public async Task SignInWithUser(string email, string password)
    {
        var response = await _httpClient.GetAsync("Account/Login");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Email", email),
                new KeyValuePair<string, string>("Password", password),
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Account/Login")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Account/Login", content);
        responseContent = await response.Content.ReadAsStringAsync();
    }

    public async Task ChangePassword(string email)
    {
        var response = await _httpClient.GetAsync("Manage/ChangePassword");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("OldPassword", "Password~1"),
                new KeyValuePair<string, string>("NewPassword", "Password~2"),
                new KeyValuePair<string, string>("ConfirmPassword", "Password~2"),
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Manage/ChangePassword")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Manage/ChangePassword", content);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains("Your password has been changed.", responseContent, StringComparison.OrdinalIgnoreCase);
        // Assert.NotNull(_httpClientHandler.CookieContainer.GetCookies(new Uri(Config.ApplicationBaseUrl)).GetCookieWithName(".AspNet.Microsoft.AspNet.Identity.Application"));
    }

    public async Task<string> CreateAlbum()
    {
        var albumName = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 12);
        var response = await _httpClient.GetAsync("Admin/StoreManager/create");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Admin/StoreManager/create")),
                new KeyValuePair<string, string>("GenreId", "1"),
                new KeyValuePair<string, string>("ArtistId", "1"),
                new KeyValuePair<string, string>("Title", albumName),
                new KeyValuePair<string, string>("Price", "9.99"),
                new KeyValuePair<string, string>("AlbumArtUrl", "http://myapp/testurl"),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Admin/StoreManager/create", content);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Equal<string>(Config.ApplicationBaseUrl + "Admin/StoreManager", response.RequestMessage.RequestUri.AbsoluteUri);
        Assert.Contains(albumName, responseContent);
        return albumName;
    }

    public async Task<string> FetchAlbumIdFromName(string albumName)
    {
        // Run some CORS validation.
        _httpClient.DefaultRequestHeaders.Add("Origin", "http://notpermitteddomain.com");
        var response = await _httpClient.GetAsync(string.Format("Admin/StoreManager/GetAlbumIdFromName?albumName={0}", albumName));
        await ThrowIfResponseStatusNotOk(response);
        IEnumerable<string> values;
        Assert.False(response.Headers.TryGetValues("Access-Control-Allow-Origin", out values));

        _httpClient.DefaultRequestHeaders.Remove("Origin");
        _httpClient.DefaultRequestHeaders.Add("Origin", "http://example.com");
        response = await _httpClient.GetAsync(string.Format("Admin/StoreManager/GetAlbumIdFromName?albumName={0}", albumName));
        await ThrowIfResponseStatusNotOk(response);
        Assert.Equal("http://example.com", response.Headers.GetValues("Access-Control-Allow-Origin").First());
        _httpClient.DefaultRequestHeaders.Remove("Origin");

        var albumId = await response.Content.ReadAsStringAsync();
        return albumId;
    }

    public async Task VerifyAlbumDetails(string albumId, string albumName)
    {
        var response = await _httpClient.GetAsync(string.Format("Admin/StoreManager/Details?id={0}", albumId));
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains(albumName, responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains("http://myapp/testurl", responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains(PrefixBaseAddress(string.Format("<a href=\"/{0}/Admin/StoreManager/Edit?id={1}\">Edit</a>", "{0}", albumId)), responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Contains(PrefixBaseAddress("<a href=\"/{0}/Admin/StoreManager\">Back to List</a>"), responseContent, StringComparison.OrdinalIgnoreCase);
    }

    public async Task VerifyStatusCodePages()
    {
        var response = await _httpClient.GetAsync("Admin/StoreManager/Details?id=-1");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains("Item not found.", responseContent, StringComparison.OrdinalIgnoreCase);
        Assert.Equal(PrefixBaseAddress("/{0}/Home/StatusCodePage"), response.RequestMessage.RequestUri.AbsolutePath);
    }

    // This gets the view that non-admin users get to see.
    public async Task GetAlbumDetailsFromStore(string albumId, string albumName)
    {
        var response = await _httpClient.GetAsync(string.Format("Store/Details/{0}", albumId));
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains(albumName, responseContent, StringComparison.OrdinalIgnoreCase);
    }

    public async Task AddAlbumToCart(string albumId, string albumName)
    {
        var response = await _httpClient.GetAsync(string.Format("ShoppingCart/AddToCart?id={0}", albumId));
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
    }

    public async Task CheckOutCartItems()
    {
        var response = await _httpClient.GetAsync("Checkout/AddressAndPayment");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();

        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Checkout/AddressAndPayment")),
                new KeyValuePair<string, string>("FirstName", "FirstNameValue"),
                new KeyValuePair<string, string>("LastName", "LastNameValue"),
                new KeyValuePair<string, string>("Address", "AddressValue"),
                new KeyValuePair<string, string>("City", "Redmond"),
                new KeyValuePair<string, string>("State", "WA"),
                new KeyValuePair<string, string>("PostalCode", "98052"),
                new KeyValuePair<string, string>("Country", "USA"),
                new KeyValuePair<string, string>("Phone", "PhoneValue"),
                new KeyValuePair<string, string>("Email", "email@email.com"),
                new KeyValuePair<string, string>("PromoCode", "FREE"),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Checkout/AddressAndPayment", content);
        responseContent = await response.Content.ReadAsStringAsync();
    }

    public async Task DeleteAlbum(string albumId, string albumName)
    {
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("id", albumId)
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        var response = await _httpClient.PostAsync("Admin/StoreManager/RemoveAlbum", content);
        await ThrowIfResponseStatusNotOk(response);

        response = await _httpClient.GetAsync(string.Format("Admin/StoreManager/GetAlbumIdFromName?albumName={0}", albumName));
        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }

    private async Task ThrowIfResponseStatusNotOk(HttpResponseMessage response)
    {
        if (response.StatusCode != HttpStatusCode.OK)
        {
            await response.Content.ReadAsStringAsync();
            throw new Exception(string.Format("Received the above response with status code : {0}", response.StatusCode));
        }
    }

    public async Task<int> GetUsersCount()
    {
        var response = await _httpClient.GetAsync("/Store/GetUsersCount/");
        string s = response.Content.ReadAsStringAsync().Result.ToString();
        return Convert.ToInt32(s);
    }
    public async Task<int> GetAlbumsCount()
    {
        var response = await _httpClient.GetAsync("/Store/GetAlbumsCount/");
        string s = response.Content.ReadAsStringAsync().Result.ToString();
        return Convert.ToInt32(s);
    }
    public async Task GetGenres()
    {
        genreList = new List<string>();

        var response = await _httpClient.GetAsync("/Store/Genre/");
        char[] ch = { '[', ']', ',', '"' };
        String[] splitArray = response.Content.ReadAsStringAsync().Result.ToString().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        string[] genreArray = splitArray.Select(x => x.Replace("R&B", "R%26B")).ToArray();
        genreList.AddRange(genreArray);
    }
    public async Task<string> CreateAlbum(int index)
    {
        var albumName = "Album" + index;
        Random rnd = new Random();
        string GenreId = rnd.Next(1, 15).ToString();
        string ArtistId = rnd.Next(1, 303).ToString();

        var response = await _httpClient.GetAsync("Admin/StoreManager/create");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Admin/StoreManager/create")),
                new KeyValuePair<string, string>("GenreId", GenreId),
                new KeyValuePair<string, string>("ArtistId",ArtistId ),
                new KeyValuePair<string, string>("Title", albumName),
                new KeyValuePair<string, string>("Price", "9.99"),
                new KeyValuePair<string, string>("AlbumArtUrl", "http://myapp/testurl"),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Admin/StoreManager/create", content);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Equal<string>(Config.ApplicationBaseUrl + "Admin/StoreManager", response.RequestMessage.RequestUri.AbsoluteUri);
        Assert.Contains(albumName, responseContent);
        return albumName;
    }
    public async Task RegisterValidUser(int i)
    {
        var response = await _httpClient.GetAsync("Account/Register");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        ValidateLayoutPage(responseContent);

        var generatedEmail = "user" + i + "@test.com";
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Email", generatedEmail),
                new KeyValuePair<string, string>("Password", "Password@"+i),
                new KeyValuePair<string, string>("ConfirmPassword", "Password@"+i),
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Account/Register")),
            };

        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("/Account/Register", content);
        responseContent = await response.Content.ReadAsStringAsync();

        //Account verification
        Assert.Equal<string>(Config.ApplicationBaseUrl + "Account/Register", response.RequestMessage.RequestUri.AbsoluteUri);
        Assert.Contains("For DEMO only: You can click this link to confirm the email:", responseContent, StringComparison.OrdinalIgnoreCase);
        var startIndex = responseContent.IndexOf("[[<a href=\"", 0) + "[[<a href=\"".Length;
        var endIndex = responseContent.IndexOf("\">link</a>]]", startIndex);
        var confirmUrl = responseContent.Substring(startIndex, endIndex - startIndex);
        confirmUrl = WebUtility.HtmlDecode(confirmUrl);
        response = await _httpClient.GetAsync(confirmUrl);
        await ThrowIfResponseStatusNotOk(response);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Contains("Thank you for confirming your email.", responseContent, StringComparison.OrdinalIgnoreCase);
    }
    private enum AlbumSearch
    {
        SearchAlbum,
        SearchAlbumAndAddToCart,
        SearchAlbumAndBuy
    }
    public async Task SearchAlbums()
    {
        await BuyAlbums(AlbumSearch.SearchAlbum);
    }
    public async Task SearchAlbumsAndAddtoCart()
    {
        await BuyAlbums(AlbumSearch.SearchAlbumAndAddToCart);
    }
    public async Task SearchAlbumsAddtoCartAndBuy()
    {
        await BuyAlbums(AlbumSearch.SearchAlbumAndBuy);
    }

    private async Task BuyAlbums(AlbumSearch albumSearch)
    {
        Random r = new Random();
        string genre = genreList.ElementAt(r.Next(genreList.Count));//genreList

        var response = await _httpClient.GetAsync("/Store/GetAlbum?Genre=" + genre);
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        //Assert.Contains(genre, responseContent, StringComparison.OrdinalIgnoreCase);

        char[] ch = { '[', ']', ',', '"' };
        String[] albumArray = response.Content.ReadAsStringAsync().Result.ToString().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        Random rnd = new Random();
        int key = rnd.Next(albumArray.Count());
        string albumId = albumArray[key];

        response = await _httpClient.GetAsync("/Store/Details/" + albumId);
        await ThrowIfResponseStatusNotOk(response);
        responseContent = await response.Content.ReadAsStringAsync();
        //Assert.Contains(albumName, responseContent, StringComparison.OrdinalIgnoreCase);

        if (albumSearch == AlbumSearch.SearchAlbumAndAddToCart)
            await AddAlbumToCart(albumId, "");

        if (albumSearch == AlbumSearch.SearchAlbumAndBuy)
        {
            await AddAlbumToCart(albumId, "");
            await CheckOutCartItems();
        }
    }
    public async Task<string> CreateAlbums(int count)
    {
        var response = await _httpClient.GetAsync("Admin/StoreManager/create");
        await ThrowIfResponseStatusNotOk(response);
        var responseContent = await response.Content.ReadAsStringAsync();
        var formParameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("__RequestVerificationToken", HtmlDOMHelper.RetrieveAntiForgeryToken(responseContent, "/Admin/StoreManager/create")),
                new KeyValuePair<string, string>("count", count.ToString()),
            };
        var content = new FormUrlEncodedContent(formParameters.ToArray());
        response = await _httpClient.PostAsync("Admin/StoreManager/AddAlbums/", content);
        responseContent = await response.Content.ReadAsStringAsync();
        Assert.Equal<string>(Config.ApplicationBaseUrl + "Admin/StoreManager", response.RequestMessage.RequestUri.AbsoluteUri);
        return "";
    }
}
