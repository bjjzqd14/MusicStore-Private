using System;
using System.Collections.Generic;
using System.Net.Http;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;

namespace MSWClient
{
    public class Clients
    {
        string _new24HrTimePattern = String.Empty;
        int _currentNumberOfUsers;
        int _rampupTime, _measurementTime;
        DateTime _rampupEndTime, _measurementEndTime, _currentTime, _lastTime;
        RunData _bag;
        Timer _rampUpTimer, _measurementTimer, _runtimeThroughputTimer;
        bool _spinThreads;
        double _throughput, _lastThroughput, _maxThroughput;
        Views _views;
        
        public Clients()
        {
            _new24HrTimePattern = CultureInfo
                .CurrentCulture
                .DateTimeFormat
                .LongTimePattern
                .Replace("h", "H")
                .Replace("t", "");
            _bag = new RunData();
            _maxThroughput = 0;
            _views = new Views();
        }
        
        private int GetMaxNumberOfUsers()
        {
            return "Auto" == Config.Mode && Config.MaxNumberOfUsers >= Config.Guests ? Config.MaxNumberOfUsers : Config.Guests;
        }
        
        private async Task UserTask(int tempUserIndex, Transactions transactions)
        {
            Sequence seq = new Sequence();
            seq.Init(tempUserIndex);
            Stopwatch stopwatch;
            
            while (_spinThreads)
            {
                List<int> sequencesInRandomOrder = transactions.RandomSequence();
                
                for (int i = 0; i < sequencesInRandomOrder.Count; i++)
                {
                    if (_spinThreads)
                    {
                        SequenceData seqRec = _bag.Add(tempUserIndex, sequencesInRandomOrder[i]);
                        foreach (TransactionType txn in transactions.allSequences[sequencesInRandomOrder[i]])
                        {
                            stopwatch = Stopwatch.StartNew();
                            await seq.Execute(txn);
                            stopwatch.Stop();
                            seqRec.Add(stopwatch.ElapsedMilliseconds, txn);
                        }
                    }
                }
            }
        }
        
        private void CollectRunData()
        {
            _bag = new RunData();
            Transactions transactions = new Transactions(Config.SequenceDistribution);
            Task [] userTasks = new Task[_currentNumberOfUsers];
            _rampupTime = Config.RampUpTime;
            _measurementTime = Config.MeasurementTime;
            _spinThreads = true;
            _rampUpTimer = new Timer(OnRampupStart, new AutoResetEvent(false), 0, -1);
            if (Config.RuntimeThroughputTime > 0)
            {
                _runtimeThroughputTimer = new Timer(RuntimeCallBack, null, 0, Config.RuntimeThroughputTime * 1000);
            }
            for (int userIndex = 0; userIndex < _currentNumberOfUsers; userIndex++)
            {
                // scope the userIndex variable and pass it to lambda http://stackoverflow.com/questions/271440/captured-variable-in-a-loop-in-c-sharp
                int tempUserIndex = userIndex;
                userTasks[tempUserIndex] = Task.Factory.StartNew(async () =>
                {
                    await UserTask(tempUserIndex, transactions);
                }).Unwrap();
            }
            Task.WhenAll(userTasks).Wait();
        }

        public async Task RunTheBenchmark()
        {
            int maxNumberOfUsers = GetMaxNumberOfUsers();
            await InitRun();
            List<RunStatistics> runData = new List<RunStatistics>();
            for (_currentNumberOfUsers = Config.Guests; _currentNumberOfUsers <= maxNumberOfUsers; _currentNumberOfUsers += Config.IncrementalUsers)
            {
                _views.BeginRunStatistics(_currentNumberOfUsers);
                CollectRunData();
                runData.Add(_bag.GetRunStatistics(_currentNumberOfUsers, _measurementEndTime, _rampupEndTime));
                _views.EndRunStatistics(runData.Last(), _bag.Validate(), _measurementEndTime);
                _throughput = runData.Last().Throughput;
                if (_throughput < _maxThroughput)
                {
                    //only do Config.RunsAfterMax more runs
                    if (_currentNumberOfUsers + (Config.RunsAfterMax * Config.IncrementalUsers) < maxNumberOfUsers)
                    {
                        maxNumberOfUsers = _currentNumberOfUsers + (Config.RunsAfterMax * Config.IncrementalUsers);
                    }
                } 
                else if (Config.Mode == "Auto" && Config.MaxNumberOfUsers >= Config.Guests)
                {
                    _maxThroughput = _throughput;
                    maxNumberOfUsers = Config.MaxNumberOfUsers;
                }
                _lastThroughput = _throughput;            
            }

            _views.FinalStatistics(runData);
        }

        private async Task InitRun()
        {
            var httpClientHandler = new HttpClientHandler();
            var httpClient = new HttpClient(httpClientHandler) 
            { 
                BaseAddress = new Uri(Config.ApplicationBaseUrl),
                Timeout = new System.TimeSpan(0, 0, 30) 
            };

            Validator validator = new Validator(httpClient, httpClientHandler);

            var response = await httpClient.GetAsync(string.Empty);

            await validator.VerifyHomePage(response);

            Sequence.setResponse(response);

            await validator.SignInWithUser(Config.DefaultAdminUsername, Config.DefaultAdminPassword);

            // do not count admin
            // 
            int count = await validator.GetUsersCount() - 1;
            int maxNumberOfUsers;
            if (Config.Mode == "Auto" && Config.MaxNumberOfUsers >= Config.Guests)
            {
                maxNumberOfUsers = Config.MaxNumberOfUsers;
            } 
            else
            {
                maxNumberOfUsers = Config.Guests;
            }

            if (maxNumberOfUsers > count)
            {
                for (int i = count; i < maxNumberOfUsers; i++)
                    await validator.RegisterValidUser(i);
            }

            count = await validator.GetAlbumsCount(); // DbUtils.GetAlbumsCount(musicStoreDbName, logger);
            if (Config.Albums > count)
            {
                int noAlbums = Config.Albums - count;
                int loop = noAlbums / 1000;
                if (loop >= 1)
                {
                    int i = 1;
                    for (i = 1; i <= loop; i++)
                    {
                        await validator.CreateAlbums(1000);
                        noAlbums = noAlbums - 1000;
                    }
                    await validator.CreateAlbums(noAlbums);
                }
                else
                {
                    await validator.CreateAlbums(noAlbums);
                }
            }

            await validator.GetGenres();
            await validator.SignOutUser("Administrator");
        }

        private void OnRampupStart(Object autoEvent_rampup)
        {
            _bag.SetRunState(RunState.Rampup);
            _lastTime = DateTime.Now;
            ((AutoResetEvent) autoEvent_rampup).WaitOne(_rampupTime * 1000);
            //On Rampup Timed Event
            _rampUpTimer.Dispose();
            _measurementTimer = new Timer(OnMeasurementStart, new AutoResetEvent(false), 0, 0);
        }

        private void OnMeasurementStart(Object autoEvent_measurement)
        { 
            _rampupEndTime = DateTime.Now;
            _bag.SetRunState(RunState.Running);
            _bag.OnMeasurementStart();
	    _views.EndRampup();
            ((AutoResetEvent) autoEvent_measurement).WaitOne(_measurementTime * 1000);
            //On Measurement Timed Event
            _measurementTimer.Dispose();
            _runtimeThroughputTimer.Dispose();
            _spinThreads = false;
            _measurementEndTime = DateTime.Now;
            _bag.SetRunState(RunState.Ended);
        }

        private void RuntimeCallBack(Object stateInfo)
        {
            _currentTime = DateTime.Now;
            RunStatistics rs = _bag.RuntimeThroughput(_currentTime, _lastTime, _currentNumberOfUsers, _new24HrTimePattern);
            if (rs != null) {
                _views.RuntimeUpdate(rs);
                if(_bag.GetRunState() == RunState.Running)
                {
                    _bag.AddIntervalStatistics(rs.Throughput, rs.ResponseTimeMin, rs.ResponseTimeMax, rs.ResponseTimeAverage, rs.ResponseTimeNinetyNinePercentile, rs.ResponseTimeStandardDeviation);
                }
            }

            _lastTime = _currentTime;
        }
    }
}
