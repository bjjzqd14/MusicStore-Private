
# About MusicStore Workload
MusicStore Workload is an end-to-end workload, which models an online music store where a typical user could login, search albums, create a shop cart and make purchase. It measures response time, throughput, and other performance metrics. The load on the Web application is incremented gradually by a Client application built on top of E2E test project. The database used by the web application can be either a separate SQL DB or in memory DB. 
Web application logic is from MusicStore sample project, open sourced on GitHub by Microsoft, to demonstrate the capabilities of the new features of ASP.NET core 1.0.

# Scope and goals

The goal of the MusicStore Workload is to evaluate the performance and scalability of environments for ASP.NET core 1.0 web applications using CoreCLR. In addition, the workload provides a flexible framework for measuring system performance, including CoreCLR performance, operating system performance and underlying hardware / system performance. The following goals considered to meet these requirements are:

**Simulate a relevant application model:** Exercise the components of the ASP.NET and CoreCLR runtime and its OS and hardware environment. Include a response time metric because response time is a major factor in service level agreements. Scale to use all available processors and memory resources.

**Scaling:** Initial version 1.0 is a single instance workload. Later versions planned are multiple instances to test multiple VMs/Containers/Guest OS instances. Single instance version is been tested for up to two socket systems.
IO Components: This workload stress network IO between client and web application and some limited IO between web application and database based on deployment configuration. However, the main intent is not to make the workload as IO bound.

**Run Time:** 
Run length varies based workload configuration. It usually takes about few minutes for a fixed run and 30-40 minutes for Auto run where load is gradually increased to find sustainable throughput. These two modes are explained in later sections. 
On fourth generation Haswell-EP  server with 18c per socket, fixed mode will take about 3 minutes and auto run takes about 30 minutes.
Multiple instance deployment is planned for the next version 2.0. The run time in this case should be less as the system resources will be shared by all instances. 

**Platforms:** 
Workload is designed and developed to evaluate performance of various platforms. It can run on various platforms and operating systems like Microsoft Windows Server, Nano Server, Containers, Linux, and Mac OS. We have tested the workload on Windows Server 2012 R2 Standard. 

**Implementation language:** 
The programming language used is C# and uses ASP.NET core 1.0 framework. 

**Think time:** Main goal for this workload is to exercise a typical ASP.NET core 1.0 web application server, running open source CoreCLR, where scaling limitation is web application server and not the clients or backend DB requests. Workload will measure response time and throughput as well as configuration flexibility to stress different components inside ASP.NET application server. 
One of the goal for this workload is to measure end-to-end responsive in sub milliseconds. Think time is in not implemented as our intent is to stress the application server. Future version of the workload plans to have a configurable think time. 

# MusicStore Workload Architecture 
The MusicStore workload application built on top of sample project MusicStore on GitHub shared by Microsoft exercises the latest ASP.net core 1.0.

### Workload Modeling
The application scenario chosen for this MusicStore Workload is a music shopping application, which allows the user to shop for music albums. The user can Login, search for albums, add albums to shopping cart, and make a payment for the items and then logout from the application.

### Workload Components
MusicStore Workload has three major components. 

![alt text](https://github.com/intel-str/MusicStore-Private/blob/master/MSWServerArchitecture.jpg "Architecture")

#### MSWClient Application
MSWClient Application is built on top of E2ETest application shared by Microsoft. This application will access the MSWServer Application hosted on kestrel HTTP server. During startup, the client will send requests to server to add more users and albums to the database. Number of users and albums are configurable. The application will spawn multiple user threads, which generates HTTP transaction requests to MSWServer Application.
There are three stages during run time. In Ramp-up time, client creates user threads, which start executing sequence of transaction in random order. 
Measurement period starts when processor utilization reaches sustainable steady state. Client calculates runtime performance values like throughput, standard deviation, 99 percentile and minimum maximum response time. Client calculates the throughput as number of sequences of transactions executed per second. 
After measurement period, the client starts closing the client threads. This is called ramp-down period.
After Ramp-down, Client creates a report with final performance numbers.

![alt text](https://github.com/intel-str/MusicStore-Private/blob/master/FixedModeChart.jpg "Fixed mode runtime chart")

The Client application has the two modes of operation: Fixed and Auto Mode. In the fixed mode, the client runs the workload with fixed number of users. In Auto Mode, the client iteratively adds the load to Server side web application. Client application modes are explained in next section.

####MSWServer Application
This web application is from MusicStore sample project, which is shared on GitHub by Microsoft, to demonstrate the capabilities of the new features of ASP.NET core 1.0, MVC 1.0 and Entity Framework 1.0.
Functionalities like GetGenre(), GetAlbumsByGenre() are added to MSWServer Application to support this workload. 
This sample web application allows the user to purchase music albums. Users can create account, login, search albums by name, genre and by artist, create a shop cart and checkout.

Web Application receives requests, interacts with the database, processes the requests and responds to client with the relevant data. The Web application runs on Kestrel HTTP server and the database used is either MS SQL server or in memory database.
ASP.NET MVC allows software developers to build a web application as a composition of three roles: Model, View and Controller. The MVC model defines web applications with 3 logic layers. Model (business layer), View (display layer) and Controller (input control).
A model represents the state of a particular aspect of the application. A controller handles interactions and updates the model to reflect a change in state of the application, and then passes information to the view. A view accepts necessary information from the controller and renders a user interface to display that information

The Entity Framework enables developers to work with data in the form of domain-specific objects and properties. With the Entity Framework, developers can work at a higher level of abstraction when they deal with data, and can create and maintain data-oriented applications with less code than in traditional applications.

**Database:** The web application can be configured to use SQL database or in memory database. Connection string in the config file defines data source, database name and authentication details. The total number of albums and users in the database can be configured at client application. There are several ways to host MusicStore database. It can run on the same server or on separate server.

**In Memory Database:** Entity Framework comes with an in-memory database provider. This is mainly used for unit testing because one can test the exact same data access code you use in production but on a fast, totally isolated, zero setup, data store.

#Workload Configuration
MusicStore Workload can run on three different configurations. 1- Tier, 2-Tier and 3-Tier. 

In 1-Tier, MSWServer Application, load-generating MSWClient Application and the database run on one system. This configuration is mainly used for developing and testing. By default, workload is configured to run on localhost.

In 2-tier, MSWServer Application and database run on SUT (System Under Test) or app-server and MSWClient Application runs on different system. App server and Client Server are connected using private network.

In 3-Tier configuration, MSWServer Application, MSWClient  Application and database run on different server. 2-Tier configuration and In-memory database (typically used for Unit testing) is used for Nano Server and containers. 

![alt text](https://github.com/intel-str/MusicStore-Private/blob/master/SystemsConfiguration.jpg "Workload Configuration")

#Workload Execution Phases
###Server and Client Execution
When MSWServer Application started for the first time, Entity Framework creates a new database with one admin user and 462 Albums with a database name defined in connection string. If In-Memory database option is selected, entity framework creates In-Memory database.

![alt text](https://github.com/intel-str/MusicStore-Private/blob/master/MusicStoreTiming.jpg "MusicStore Workload timing diagram")

When MSWClient Application on load-server started, it reads configuration values and tries to connect to the MSWServer Application. If it fails, it retries for 50 times with 10 seconds sleep time in between.

After verifying homepage, Client sends requests to create more users and Albums. Then it spawns n number of threads, which is equal to the number of users. Each thread will start executing sequences of transactions in random order according to sequence distribution in a loop. This is called ramp-up period. When sustainable maximum throughput is reached, client starts recording performance details. This time is called Measurement Period. After measurement period, Client’s threads are closed during Ramp-Down Period.
MSWClient Application has two modes of operation. Fixed mode and Auto mode. 

###Client: Fixed Mode Execution
In Fixed mode, fixed number of user threads are created. After completing the run, client program exits.

![alt text](https://github.com/intel-str/MusicStore-Private/blob/master/FixedModeFlowChart.jpg "Fixed Mode Flow Chart")

###Client: Auto Mode Execution 
Auto Mode, the client iteratively adds the load to Server side web application as shown below. 

It keeps track of maximum throughput achieved during every iteration. When throughput continues to fall after three iteration, it stops and creates a report.  Starting number of threads, number of threads to add during iteration are configurable.
Following flow chart explains the control flow in Auto mode.

![alt text](https://github.com/intel-str/MusicStore-Private/blob/master/AutoModeFlowChart.jpg "Auto Mode Flow Chart")


###Workload Execution Algorithm 
```
for each task
  while (in ramp-up or measurement period)
    // Get sequences of transactions in random order
    // Sequence Distribution defines occurrence of different sequences
    //
     Sequences = GetRandomSequenceAccordingToSeqDist()
     for each seq in sequences
       Execute seq
     end
  end
end
```

#Workload Requests and Transactions
###Users and Albums
MSWServer Application adds admin user and 462 Albums during startup. More users and albums can be added from MSWClient application by modifying values in config.json file. 

###Transactions
Transaction is a unit of work. The available transactions are
•	Homepage: visit homepage
•	Login: Login to MSWServer Application
•	Logout: Logout of MSWServer Application
•	Search:  search albums by genre
•	searchAndAddToCart: This transaction allows the user to search and add the album to the shopping cart.
•	SearchAndBuy: This transaction allows the user to search, add the album to shopping cart and make a payment for the same.

###Sequence
Sequence is a logical order of transactions.  Following are some examples of sequences.
*. Sequence 0:    homepage, search
*. Sequence 1:    Login, Search, and Logout
*. Sequence 2:    Login, SearchAndBuy, and Logout

###Distribution ratio
Transaction distribution defines number of occurrences of sequences. This helps to make workload more meaningful. For example, if distribution ratio is {2, 3, 5} then every user thread has a mix of 2 sequence0, 3 sequence1 and 5 sequence2. To make it realistic, these sequences are executed in random order. 

#Performance Metrics Computation
###Throughput:
Throughput is calculated as number of sequences completed during measurement time period per second.

```Throughput = (total number of sequences completed during measurement time)/(measurement time)```

###Response Time (milliseconds): Minimum, Maximum, Mean
Response time is the time taken to execute one sequence by a client thread.
Minimum, maximum and average amount of time required to complete sequence of transactions.

###Standard Deviation and 99 percentile.
The standard deviation is a measure that is used to quantify the amount of variation or dispersion of a set of data values. 

###Run validation
Execution of sequences of transaction for a given distribution ratio is validated at the end of the run.

#Configuration Settings (Client)
```
Config.json in Client application

{
  "AppSettings": {
    "SiteTitle": "ASP.NET MVC Music Store"
  },
  "DefaultAdminUsername": "Administrator@test.com",
  "DefaultAdminPassword": "YouShouldChangeThisPassword1!",
  "Mode": "Fixed", //Fixed or Auto
  "DefaultNoOfUsers": "2",     // Number of users needed for the workload run
  "DefaultNoOfAlbums": "2000", // Number of albums to add to MusicStore database
  "applicationBaseUrl": "http://10.20.30.10:8080/", // MusicStore URL with Port number
  "Verbose": false, // Output detailed log on console
  "RampUpTime": "10", // Rampup time Seconds
  "MeasurementTime": "20", // Measurement time Seconds
  "RuntimeThroughputTime": "5", //Output runtime measurements every 5 Seconds
  "TotalNoOfSequences": "3", // Three sequences defined
  "SequenceDistribution": "3,2,1", // Sequence Distribution Ratio
  "Sequence0": "login,logout",
  "Sequence1": "login,search,logout",
  "Sequence2": "login,searchAndBuy,logout",
  "ResultFolder": "Results", // write output to this folder
  "Visualizer": false, // NamedPipeClientStream "MSWMusicStorePipes" 
                       // to get runtime throughput
  // Values related to "auto" mode
  "MaxNumberOfUsers": 8, // Auto Run: Max number of users
  "IncrementalUsers": 2, // Iteration increment
  "RunsAfterMax": 3 //Stop if throughput keeps falling after 3 iterations
}
```

#Terminologies Used

#####MSWServer
MSWServer is MSWServer Application project name.
#####MSWClient
MSWClient is MSWClient Application project name.
#####SUT
System under Test
#####User Threads
Number of users/threads needed to generate load on the MSWServer application. This number of users is configured in Config.json. Once the MSWClient begins, it sends a request to MSWServer to add users to database. Each user thread then begins executing a sequence of transactions. 
Albums 
Number of albums to add to MusicStore database. This number of albums is configured in Client’s Config.json. When started, it sends a request to MSWServer Application to add albums to database.
#####Ramp-Up Period
This is the amount of time needed to start all the user threads and reach steady state. This value is configurable and read from Config.json.
#####Measurement Period
This is the amount of time set to measure different performance metrics.  This value is configurable and read from Config.json.
#####Transactions
These are the single request we make to MSWServer Application. 
Examples are Login, Logout, search for album, add album to cart, etc.
#####Sequence of Transactions
Each sequence is a mix of transaction. The sequences can be configured in Config.json file 
Example:
Sequence 0:    homepage, search
Sequence 1:    Login, Search, and Logout
Sequence 2:    Login, SearchAndBuy, and Logout
Response Time
#####Response time 
Response time is the time taken to execute one sequence by a client thread
#####Standard Deviation and 99 percentile.
The standard deviation is a measure that is used to quantify the amount of variation or dispersion of a set of data values
#####Transactions
Transaction is a unit of work. The available transactions are Homepage, Login, Logout, Search, searchAndAddToCart, SearchAndBuy
#####Fixed Mode (client)
In Fixed mode, fixed number of user threads are created to generate workload.
#####Auto Mode (Client)
The client iteratively adds the load to Server side web application as shown below. It keeps track of maximum throughput achieved during every iteration. When throughput continues to fall after three iteration, it stops and creates a report.
